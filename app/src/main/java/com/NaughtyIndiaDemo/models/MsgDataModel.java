package com.NaughtyIndiaDemo.models;

public class MsgDataModel {


    String msg;
    String date;
    String views;

    public MsgDataModel(String msg, String date, String views) {
        this.msg = msg;
        this.date = date;
        this.views=views;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }
}
