package com.NaughtyIndiaDemo.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.NaughtyIndiaDemo.R;
import com.NaughtyIndiaDemo.activities.FullmsgActvity;

import java.util.ArrayList;

public class MessagListningAdapter extends RecyclerView.Adapter<MessagListningAdapter.MyViewHolder> {

    private ArrayList<String> msgFromServer;
    private static final String font = "Billabong.ttf";
    private Context ctx;
    public  class MyViewHolder extends RecyclerView.ViewHolder {

       private TextView text_msg,text_total_views,text_date;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.text_msg =  itemView.findViewById(R.id.textViewName);
            this.text_date =  itemView.findViewById(R.id.posted_date);
            this.text_total_views =  itemView.findViewById(R.id.totalviews);
            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    // get position
                    int pos = getAdapterPosition();
                    // check if item still exists
                    if(pos != RecyclerView.NO_POSITION){
                        Intent i =new Intent(ctx, FullmsgActvity.class);
                        i.putExtra("click_msg_fullview",msgFromServer.get(pos));
                        ctx.startActivity(i);
                    }
                }
            });
        }
    }

    public MessagListningAdapter(ArrayList<String> msgFromServer, Context ctx) {
        this.msgFromServer = msgFromServer;
        this.ctx=ctx;
    }

    @Override
    public MessagListningAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.msg_listning_cardview, parent, false);

        MessagListningAdapter.MyViewHolder myViewHolder = new MessagListningAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MessagListningAdapter.MyViewHolder holder, final int listPosition) {

        TextView text_msg = holder.text_msg;
        TextView text_date = holder.text_date;
        TextView text_total_views = holder.text_total_views;
        final Typeface typeface = Typeface.createFromAsset(ctx.getAssets(), font);
        text_msg.setTypeface(typeface);
      //Log.e("MSG-",msgFromServer.get(listPosition));
        text_msg.setText(msgFromServer.get(listPosition).replaceAll("( +)"," ").trim());
       // text_date.setText(msgdataSet.get(listPosition).getDate());
       // text_total_views.setText(msgdataSet.get(listPosition).getViews());
    }

    @Override
    public int getItemCount() {
        return msgFromServer.size();
    }
}
