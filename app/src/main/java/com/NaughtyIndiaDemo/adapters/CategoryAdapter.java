package com.NaughtyIndiaDemo.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.NaughtyIndiaDemo.R;
import com.NaughtyIndiaDemo.activities.MainActivity;
import com.NaughtyIndiaDemo.activities.MessageListningActivity;
import com.NaughtyIndiaDemo.models.CatDataModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private ArrayList<Object> arraylist_subcat_name;
    HashMap<String, String> subcategoryID_HasmapList;;
    private static final String font = "Billabong.ttf";
    Context ctx;
    public  class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        ImageView imageViewIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName =  itemView.findViewById(R.id.textViewName);
            this.imageViewIcon =  itemView.findViewById(R.id.imageView);
            // on item click
            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    // get position
                    int pos = getAdapterPosition();
                    if(pos != RecyclerView.NO_POSITION){
                        Intent i =new Intent(ctx, MessageListningActivity.class);
                        i.putExtra("click_category_name",arraylist_subcat_name.get(pos).toString());
                        i.putExtra("hasmaplist_cat",subcategoryID_HasmapList);
                        ctx.startActivity(i);
                    }
                }
            });
        }
    }

    public CategoryAdapter(Context ctx, ArrayList<Object>  arraylist_subcat_name, HashMap<String, String> subcategoryID_HasmapList) {
        this.arraylist_subcat_name = arraylist_subcat_name;
        this.ctx=ctx;
        this.subcategoryID_HasmapList=subcategoryID_HasmapList;
    }

    @Override
    public CategoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cards_layout, parent, false);

        CategoryAdapter.MyViewHolder myViewHolder = new CategoryAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final CategoryAdapter.MyViewHolder holder, final int listPosition) {

        TextView textViewName = holder.textViewName;
        ImageView imageView = holder.imageViewIcon;
        final Typeface typeface = Typeface.createFromAsset(ctx.getAssets(), font);
        textViewName.setTypeface(typeface);
        textViewName.setText(arraylist_subcat_name.get(listPosition).toString());

        //imageView.setImageResource(catdataSet.get(listPosition).getImage());
    }

    @Override
    public int getItemCount() {
        return arraylist_subcat_name.size();
    }
}
