package com.NaughtyIndiaDemo.adapters;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;


import com.NaughtyIndiaDemo.R;


import com.NaughtyIndiaDemo.activities.CategoryActivity;

import com.google.common.collect.Multimap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {
    private Multimap<String, String> multiMaphsmap;
    private ArrayList<String> idlist ;
    ArrayList<String> name_list;
    private static final String font = "Billabong.ttf";
    private Context ctx;
    private ArrayList<String> subcategoryList;
    HashMap<String, String> subcategoryID_HasmapList;
    public static String ID;
        public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        ImageView imageViewIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = itemView.findViewById(R.id.textViewName);
            this.imageViewIcon = itemView.findViewById(R.id.imageView);
            // on item click
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // get position
                    int pos = getAdapterPosition();

                    // check if item still exists
                    if (pos != RecyclerView.NO_POSITION) {
                        subcategoryList.clear();

                        for (Map.Entry<String, String> entry : multiMaphsmap.entries()) {
                            String key = entry.getKey(), value = entry.getValue();
                            if (key.equalsIgnoreCase(idlist.get(pos))) {
                                subcategoryList.add(entry.getValue());

                            }
                        }

                        Intent i =new Intent(ctx, CategoryActivity.class);
                        Bundle args = new Bundle();
                        args.putSerializable("ARRAYLIST",(Serializable) subcategoryList);
                        i.putExtra("hasmaplist",subcategoryID_HasmapList);
                        i.putExtra("BUNDLE",args);
                        Log.e("ID--",idlist.get(pos));
                        ID=idlist.get(pos);
                        ctx.startActivity(i);
                    }

                }
            });
        }
    }

    public CustomAdapter(Context ctx,   ArrayList<String> name_list ,ArrayList<String> idlist, Multimap<String, String> multiMaphsmap,HashMap<String, String> subcategoryID_HasmapList) {
        this.name_list = name_list;
        this.ctx = ctx;
        this.idlist=idlist;
        this.multiMaphsmap=multiMaphsmap;
        this.subcategoryID_HasmapList=subcategoryID_HasmapList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cards_layout, parent, false);
        subcategoryList=new ArrayList<>();
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textViewName = holder.textViewName;
        ImageView imageView = holder.imageViewIcon;
        final Typeface typeface = Typeface.createFromAsset(ctx.getAssets(), font);
        textViewName.setTypeface(typeface);
        textViewName.setText(name_list.get(listPosition));
        Log.e("Size of list--", name_list.size() + "");
        //imageView.setImageResource(dataSet.get(listPosition).getImage());
    }

    @Override
    public int getItemCount() {
        return name_list.size();
    }
}
