package com.NaughtyIndiaDemo.activities;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.NaughtyIndiaDemo.R;
import com.NaughtyIndiaDemo.adapters.CustomAdapter;
import com.android.volley.VolleyError;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    Multimap<String, String> multiMaphsmap = ArrayListMultimap.create();
    static HashMap<String, String> subcategoryID_HasmapList = new HashMap<>();
    ArrayList<String> idlist = new ArrayList<>();
    ArrayList<String> name_list = new ArrayList<>();
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView recyclerView;
    IResult mResultCallback;
    CatLoadingView dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        dialog = new CatLoadingView();

      //  Call Volly TO Fetch Data From Server CatLoadin Dialog
        dialog.show(getSupportFragmentManager(), "");
        try {
            initVolleyCallback();
            VolleyService   mVolleyService = new VolleyService(mResultCallback,MainActivity.this);
            mVolleyService.postDataVolley(0,"");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void initVolleyCallback(){
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String  response) {
                Log.e("response",  response);
                JSONObject obj = null;
                JSONObject heroObject = null;
                try {
                    obj = new JSONObject(response);
                    JSONArray responseData = obj.getJSONArray("responseData");
                    for (int i = 0; i < responseData.length(); i++) {
                        //getting the json object of the particular index inside the array
                        heroObject = responseData.getJSONObject(i);
                        String cat_name = heroObject.getString("category_name");
                        name_list.add(cat_name);
                        String id = heroObject.getString("id");
                        idlist.add(id);
                        Log.e("ALL ID-", id);
                        JSONArray subcatarray = heroObject.getJSONArray("subcategory_list");
                        for (int j = 0; j < subcatarray.length(); j++) {
                            JSONObject suboject = subcatarray.getJSONObject(j);
                            String category_id = suboject.getString("category_id");
                            Log.e("category_id", category_id);
                            String subcatname = suboject.getString("subcategory_name");
                            String subid = suboject.getString("id");
                            Log.e("id", subid);
                            subcategoryID_HasmapList.put(subid, subcatname);
                            HaspMapSetMethod(category_id, subcatname);

                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (dialog.isAdded()) {
                    dialog.dismiss();
                }

                //dismiss CatDialog is showing already
                if (dialog.isAdded())dialog.dismiss();

                adapter = new CustomAdapter(MainActivity.this, name_list, idlist, multiMaphsmap,subcategoryID_HasmapList);
                recyclerView.setAdapter(adapter);
              //

            }

            @Override
            public void notifyError(VolleyError error) {
               error.printStackTrace();
                //dismiss CatDialog is showing already
                if (dialog.isAdded())dialog.dismiss();
            }
        };
    }



    private void HaspMapSetMethod(String category_id, String subcatname) {

        multiMaphsmap.put(category_id, subcatname);

    }
}
