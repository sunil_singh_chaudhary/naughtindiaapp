package com.NaughtyIndiaDemo.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.NaughtyIndiaDemo.R;
import com.android.volley.VolleyError;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class PostActivity extends AppCompatActivity {
    EditText post_msg;
    TextView post_date, userName_textv;
    ArrayList<String> username_list = new ArrayList<>();
    ArrayList<String> userid_ist = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    String ursID;
    Menu mMenu;
    CatLoadingView dialog,catLoadingdialog2;
    IResult mResultCallback,mResultCallback_postmsg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        Init();

        SharedpreferenceForResume();

        post_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenCalender();

            }
        });

        userName_textv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMenu.findItem(R.id.post_item).setEnabled(false);
                dialog = new CatLoadingView();
                //  Call Volly TO Fetch Data From Server CatLoadin Dialog
                dialog.show(getSupportFragmentManager(), "");
                try {
                    DetailAllUserAsync();
                    VolleyService   mVolleyService = new VolleyService(mResultCallback,PostActivity.this);
                    mVolleyService.postDataVolley(2,"");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void Init() {

        post_date = findViewById(R.id.post_date);
        post_msg = findViewById(R.id.post_msg);
        userName_textv = findViewById(R.id.userName);
        arrayAdapter = new ArrayAdapter<String>(PostActivity.this, android.R.layout.select_dialog_singlechoice);





    }

    private void SharedpreferenceForResume() {
        SharedPreferences  sharedPreferences = getSharedPreferences("WHERE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("from_where",1);
        editor.apply();
    }

    private void DetailAllUserAsync() {

        mResultCallback=new IResult() {
            @Override
            public void notifySuccess(String response) {
                JSONObject obj = null;
                JSONObject heroObject = null;
                try {
                    obj = new JSONObject(response);
                    JSONArray responseData = obj.getJSONArray("responseData");
                    for (int i = 0; i < responseData.length(); i++) {
                        heroObject = responseData.getJSONObject(i);
                        String post_id = heroObject.getString("id");
                        String post_name = heroObject.getString("name");
                        //   Log.e("category_msg_base64",cat_msg);
                        arrayAdapter.add(post_name);
                        username_list.add(post_name);
                        userid_ist.add(post_id);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mMenu.findItem(R.id.post_item).setEnabled(true);
                dialog.dismiss();
                setUpAlerDialog();
            }

            @Override
            public void notifyError(VolleyError error) {

            }
        };
        mMenu.findItem(R.id.post_item).setEnabled(true);
    }



    private void setUpAlerDialog() {


        AlertDialog.Builder builderSingle = new AlertDialog.Builder(PostActivity.this);
        builderSingle.setIcon(R.drawable.ic_launcher);
        builderSingle.setTitle("Select User Name:-");

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                ursID = userid_ist.get(which);
                Log.e("strid--", ursID + "");
                Log.e("strname--", username_list.get(which) + "");
                userName_textv.setText(username_list.get(which));

            }
        });
        builderSingle.show();
    }

    private void OpenCalender() {

        // Initialize
        SwitchDateTimeDialogFragment dateTimeDialogFragment = SwitchDateTimeDialogFragment.newInstance(
                "Choose",
                "OK",
                "Cancel"
        );

       // Assign values
        dateTimeDialogFragment.startAtCalendarView();
        dateTimeDialogFragment.set24HoursMode(true);
        dateTimeDialogFragment.setMinimumDateTime(new GregorianCalendar(2017, Calendar.JANUARY, 1).getTime());
        dateTimeDialogFragment.setMaximumDateTime(new GregorianCalendar(2022, Calendar.DECEMBER, 31).getTime());

       // Define new day and month format
        try {
            dateTimeDialogFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("yyyy-MM-dd HH:mmMM:ss", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e("error", e.getMessage());
        }

       // Set listener
        dateTimeDialogFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {
                // Date is get on positive button click
                // Do something
                SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String d = targetFormat.format(date);
                post_date.setText(d);
            }

            @Override
            public void onNegativeButtonClick(Date date) {
                // Date is get on negative button click
            }
        });

        // Show
        dateTimeDialogFragment.show(getSupportFragmentManager(), "dialog_time");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.post_menu_main, menu);
        mMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.post_item:
                mMenu.findItem(R.id.post_item).setEnabled(false);
                catLoadingdialog2 = new CatLoadingView();
                //  Call Volly TO Fetch Data From Server CatLoadin Dialog
                catLoadingdialog2.show(getSupportFragmentManager(), "");
                try {

                    String main_id = getIntent().getStringExtra("main_id");
                    String sb_id = getIntent().getStringExtra("sb_id");
                    String created_date = post_date.getText().toString();
                    String msg = post_msg.getText().toString();
                    String title = " ";
                    String posted_by_user_id = ursID;

                    postToServerDataVolly();
                    VolleyService   mVolleyService = new VolleyService(mResultCallback_postmsg,PostActivity.this,main_id,sb_id,created_date,msg,title,posted_by_user_id);
                    mVolleyService.postDataVolley(3,"");


                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void postToServerDataVolly() {
        mResultCallback_postmsg =new IResult() {
            @Override
            public void notifySuccess(String response) {
                Toast.makeText(getApplicationContext(), "Posted Successfully", Toast.LENGTH_LONG).show();
                catLoadingdialog2.dismiss();
                mMenu.findItem(R.id.post_item).setEnabled(true);
                post_msg.setText( "" );
                post_date.setText("");
                userName_textv.setText( "" );
            }

            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(getApplicationContext(), "Un-Successful", Toast.LENGTH_LONG).show();

            }
        };
    }



}
