package com.NaughtyIndiaDemo.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.NaughtyIndiaDemo.adapters.CategoryAdapter;
import com.NaughtyIndiaDemo.R;
import com.NaughtyIndiaDemo.models.CatData;
import com.NaughtyIndiaDemo.models.CatDataModel;

import java.util.ArrayList;
import java.util.HashMap;

public class CategoryActivity extends AppCompatActivity {

    private static RecyclerView.Adapter cat_adapter;
    private RecyclerView.LayoutManager cat_layoutManager;
    private static RecyclerView cat_recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_catigory);
        Bundle args = getIntent().getBundleExtra("BUNDLE");
        ArrayList<Object> arraylist_subcat_name = (ArrayList<Object>) args.getSerializable("ARRAYLIST");
     //   ArrayList<Object> hasmapList = (ArrayList<Object>) args.getSerializable("hasmaplist");
        Intent intent = getIntent();
        HashMap<String, String> hasmapList = (HashMap<String, String>)intent.getSerializableExtra("hasmaplist");




        cat_recyclerView =  findViewById(R.id.cat_recycler_view);
        cat_recyclerView.setHasFixedSize(true);
        cat_layoutManager = new LinearLayoutManager(this);
        cat_recyclerView.setLayoutManager(cat_layoutManager);
        cat_recyclerView.setItemAnimator(new DefaultItemAnimator());
        cat_adapter = new CategoryAdapter(CategoryActivity.this,arraylist_subcat_name,hasmapList);
        cat_recyclerView.setAdapter(cat_adapter);
    }
}
