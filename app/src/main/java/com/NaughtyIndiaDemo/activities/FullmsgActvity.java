package com.NaughtyIndiaDemo.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.NaughtyIndiaDemo.R;

public class FullmsgActvity extends AppCompatActivity {
    TextView fullmsg_textview;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullmsg_actvity);

        String pageName = getIntent().getStringExtra("click_msg_fullview");
        Log.e("cat name--", pageName);
        fullmsg_textview=findViewById(R.id.fullmsg_textview);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        fullmsg_textview.setText(pageName);

        sharedPreferences = getSharedPreferences("WHERE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("from_where",0);
        editor.apply();
    }
}
