package com.NaughtyIndiaDemo.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.NaughtyIndiaDemo.R;
import com.NaughtyIndiaDemo.adapters.CustomAdapter;
import com.NaughtyIndiaDemo.adapters.MessagListningAdapter;
import com.android.volley.VolleyError;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MessageListningActivity  extends AppCompatActivity {
    private static RecyclerView.Adapter msg_adapter;
    private RecyclerView.LayoutManager msg_layoutManager;
    private static RecyclerView msg_recyclerView;
    public static String subCatergoryKey = null;
    private  ArrayList<String> msgFromServer=new ArrayList<>();
    private  CatLoadingView cat_dialog;
    private  IResult mResultCallback;
    private HashMap<String, String> hasmapList;
    private String sub_cat_name;
    SharedPreferences pref;
    String msg_count;
    MenuItem count;
    Menu menu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_listning);
        sub_cat_name = getIntent().getStringExtra("click_category_name");
        hasmapList = (HashMap<String, String>)getIntent().getSerializableExtra("hasmaplist_cat");
        pref = getSharedPreferences("WHERE", MODE_PRIVATE);
        //  Call Volly TO Fetch Data From Server CatLoadin Dialog
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("from_where",0);
        editor.apply();

        cat_dialog = new CatLoadingView();
        cat_dialog.show( getSupportFragmentManager(), "2" );

        GetHasmapSubCategory(savedInstanceState);

        SetRecyclerView();

        ShowVollyDialog("2");

    }

    private void GetHasmapSubCategory(Bundle savedInstanceState) {

        //GET SUBCATEGORY KEY FROM HASMAP
        for(Map.Entry entry: hasmapList.entrySet()){
            if(sub_cat_name.equals(entry.getValue())){
                subCatergoryKey = entry.getKey().toString();
                break;
            }
        }
        Log.e("ID",CustomAdapter.ID+"");
        Log.e("sub_ID 2--",subCatergoryKey);

        if (savedInstanceState != null) {
            subCatergoryKey = savedInstanceState.getString("subCatergoryKey");
            Log.e("saveInstance sub_ID--",subCatergoryKey);
        }
    }

    private void SetRecyclerView() {
        msg_recyclerView =  findViewById(R.id.msg_recycler_view);
        msg_recyclerView.setHasFixedSize(true);
        msg_layoutManager = new LinearLayoutManager(this);
        msg_recyclerView.setLayoutManager(msg_layoutManager);
        msg_recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void ShowVollyDialog(String val) {


        try {
            initVolleyCallback();
            VolleyService   mVolleyService = new VolleyService(mResultCallback,MessageListningActivity.this);
            mVolleyService.postDataVolley(1,subCatergoryKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initVolleyCallback() {
        mResultCallback=new IResult() {
            @Override
            public void notifySuccess(String response) {
                Log.e( "Response MSG-",response );
                msgFromServer.clear();
                JSONObject obj = null;
                JSONObject heroObject = null;
                try {
                    obj = new JSONObject(response);
                    msg_count = obj.getString("total_msg");
                    invalidateOptionsMenu();
                    Log.e( "TOTAL MSG--",msg_count );
                    JSONArray responseData = obj.getJSONArray("responseData");
                    for (int i = 0; i < responseData.length(); i++) {
                        heroObject = responseData.getJSONObject(i);
                        String cat_msg = heroObject.getString("message");

                        msgFromServer.add(cat_msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                msg_adapter = new MessagListningAdapter(msgFromServer, MessageListningActivity.this);
                msg_recyclerView.setAdapter(msg_adapter);
                //dismiss CatDialog is showing already
                cat_dialog.dismiss();
            }

            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();
                //dismiss CatDialog is showing already
               cat_dialog.dismiss();
            }
        };
    }

    @Override
    protected void onStart() {
        invalidateOptionsMenu();
        int value= pref.getInt("from_where", 0);
        Log.e( "FULL CHECK-",value+"" );
        if (value==0){
            Log.e( "No ","Refresh here" );
        }else if (value==1){
            ShowVollyDialog("3");
            Log.e( "YES ","Refresh here" );
        }
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.menu=menu;
        count = menu.findItem(R.id.count_msg_menu_item);
        count.setTitle(msg_count);
        invalidateOptionsMenu();
        Log.e( "onCreateOptionsMenu ",""+msg_count );
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.count_msg_menu_item);
        Log.e( "onPrepareOptionsMenu ",""+msg_count );
        item.setTitle(msg_count);
        return super.onPrepareOptionsMenu(menu);
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("subCatergoryKey",subCatergoryKey);
        outState.putString("MainID", CustomAdapter.ID);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        subCatergoryKey = savedInstanceState.getString("subCatergoryKey");

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add_item:
                Intent i=new Intent(MessageListningActivity.this,PostActivity.class);
                i.putExtra("main_id",CustomAdapter.ID);
                i.putExtra("sb_id",subCatergoryKey);
                startActivity(i);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}


